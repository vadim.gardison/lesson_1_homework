// FIRST TASK

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomColorFirst(r, g, b) {
  console.log(r, g, b);
  return 'rgb(' + r + ',' + g + ',' + b + ')';
}

function toHex(c){
  if (c > 15) { return c.toString(16) }
  else { return "0" + c.toString(16) }
}

function getRandomColorSecond(r, g, b) {
  return '#' + toHex(r) + toHex(g) + toHex(b)
}

function applyElColorFirst() {
  var elementColor = getRandomColorFirst(getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257));
  firstContainer.style.backgroundColor = elementColor;
  p.innerText = elementColor;
}

function applyElColorSecond() {
  var elementColorSecond = getRandomColorSecond(getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257), getRandomIntInclusive(0, 257));
  secondContainer.style.backgroundColor = elementColorSecond;
  pS.innerText = elementColorSecond;
}

var mainDiv = document.getElementById('app');
mainDiv.style.height = '-webkit-fill-available';

// containers
var firstContainer = document.createElement('div');
mainDiv.appendChild(firstContainer);
var secondContainer = document.createElement('div');
mainDiv.appendChild(secondContainer);

// Buttons
var firstButt = document.createElement('button');
firstContainer.appendChild(firstButt);
firstContainer.style.height = '20em';
firstButt.style.marginLeft = '45%';
firstButt.style.marginRight = '45%';
firstButt.innerText = 'Press me to change bg color';
firstButt.setAttribute('onClick', 'applyElColorFirst()');
firstButt.setAttribute('id', 'butt');

var secondButt = document.createElement('button');
secondContainer.appendChild(secondButt);
secondContainer.style.height = '20em';
secondButt.style.marginLeft = '45%';
secondButt.style.marginRight = '45%';
secondButt.innerText = 'Press me to change bg color';
secondButt.setAttribute('onClick', 'applyElColorSecond()');
secondButt.setAttribute('id', 'buttSec');

// p
var p = document.createElement('p');
p.style.margin = 'auto';
p.style.width = '10%';
p.style.backgroundColor = 'antiquewhite';
firstContainer.appendChild(p);
var pS = document.createElement('p');
pS.style.margin = 'auto';
pS.style.width = '10%';
pS.style.backgroundColor = 'antiquewhite';
secondContainer.appendChild(pS);

// onload event
window.onload = function() {
  applyElColorFirst();
  applyElColorSecond();
}



// SECOND TASK

var divApp = document.getElementById('App');
var header = document.createElement('header');
divApp.appendChild(header);
var a1 = document.createElement('a');
a1.setAttribute('href', 'http://google.com.ua');
a1.setAttribute('target', '_blank');
header.appendChild(a1);
var image = document.createElement('img');
image.setAttribute( 'src', 'https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png')
a1.appendChild(image);
var divMenu = document.createElement('div');
divMenu.className = 'menu';
header.appendChild(divMenu);
var elArr = ['a2', 'a3', 'a4'];
var brArr = ['br1', 'br2', 'br3'];
var elArrLength = elArr.length;
for (let i = 1; i <= elArrLength; i++) {
  elArr[i] = document.createElement('a');
  elArr[i].setAttribute('href', `#${i}`);
  elArr[i].innerText = ` link ${i} `;
  divMenu.appendChild(elArr[i]);
  brArr[i] = document.createElement('br');
  divMenu.appendChild(brArr[i]);
  
}

/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore


  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/

/*
    Задание 2.

    Сложить в элементе с id App следующую размету HTML:

    <header>
      <a href="http://google.com.ua">
        <img src="https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png">
      </a>
      <div class="menu">
        <a href="#1"> link 1</a>
        <a href="#1"> link 2</a>
        <a href="#1"> link 3</a>
      </div>
    </header>


    Используя следующие методы для работы:
    getElementById
    createElement
    element.innerText
    element.className
    element.setAttribute
    element.appendChild

*/
